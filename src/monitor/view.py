#coding:utf-8
from django.shortcuts import render
from django.http.response import HttpResponse
from django.http import JsonResponse
import subprocess 
def index(request):
    context          = {}
    context['hello'] = 'Hello World!'
    return render(request, 'index.html', context)

def reload_deploy(request, *call_args):
    type = request.GET.get('type')
    cmd = '' # 全路径或者./相对路径  
    if type == 'deploy_www':
        cmd = '/data/hechaojie/deploy/git_deploy.sh';
    elif type == 'deploy_monitor':
         cmd = 'source /data/monitor/pull.sh';
    elif type == 'deploy_nginx':
         cmd = 'service nginx reload';
    else :
        cmd = '';
        
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)  
    result = '';
    while p.poll() == None:  
         line = p.stdout.readline()  
         print line # 必须执行print，否则一直不返回，原因不明  
         result = result + line  
    return JsonResponse('发布完成', safe=False)